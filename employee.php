<?php 
    include "back/functions.php"; 
    $theData = new employee();


    if(isset($_POST["add_class"])) {
        
        $class_name   = mysqli_real_escape_string($conn , $_POST["class_name"]);
        $class_time   = mysqli_real_escape_string($conn ,$_POST["class_time"] );
        $class_doctor = mysqli_real_escape_string($conn , $_POST["class_doctor"]);
        $class_room   = mysqli_real_escape_string($conn , $_POST["class_room"]);

        $theData->addClass($class_name, $class_time, $class_doctor, $class_room);

    }

    if (isset($_POST["remove_class"])) {
        $id = mysqli_real_escape_string($conn , $_POST["id"]);

        $theData->removeClass($id);
    }


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>employee</title>


    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">

</head>
<body>

<?php include "inc/nav.php"; ?>

<div class="container">


    <div class="col-lg-12 my-5">

        <?php $theData->getClass(); ?>
            
    </div>

    <div class="col-lg-12 my-5">
        <div class="border-bottom my-4">
            <h1>Add a new Lesson for this Semister</h1>
        </div>
        <form>
            <div class="form-group row">
                <label for="class_name" class="col-sm-3 h3">class name</label>
                
                <input type="text" class="form-control form-control-lg col-sm-8" name="class_name" id="class_name" placeholder="class name">
                
            </div>
            

            <div class="form-group row">
                <label for="class_time" class="col-sm-3 h3">class time</label>
                
                <select name="class_time" class="form-control form-control-lg col-sm-8" id="class_time">
                    <option value="">class time</option>
                    <?php $theData->getTime(); ?>
                </select>
            </div>
            

            <div class="form-group row">
                <label for="class_doctor" class="col-sm-3 h3">class doctor</label>
                
                <select name="class_doctor" class="form-control form-control-lg col-sm-8" id="class_doctor">
                    <option value="">class doctor</option>
                    <?php $theData->getDoctor(); ?>
                </select>
            </div>
            

            <div class="form-group row">
                <label for="class_room" class="col-sm-3 h3">class room</label>
                
                <select name="class_room" class="form-control form-control-lg col-sm-8" id="class_room">
                    <option value="">class room</option>
                    <?php $theData->getRoom(); ?>
                </select>
            </div>
           
            
            <div class="form-group">
                <button type="button" class="form-control w-50 mx-auto btn-outline-success" name="add_class" id="add_class">add the class</button>
            </div>
            


        </form>

        
    </div>

    <div class="text-center my-5">
        <a href="index.php" class="btn btn-danger w-25">Back</a>
    </div>

</div>






</body>
</html>


<script>

    $(document).ready(function(){


        // it will add a class to the data base
        $("#add_class").click(function(){

            if ($.trim($("#class_name").val()).length >= 1) {
                var class_name = $("#class_name").val();
            } else {
                var class_name = "";
            }

            if ($.trim($("#class_time").val()).length >= 1) {
                var class_time = $("#class_time").val();
            } else {
                var class_time = "";
            }

            if ($.trim($("#class_doctor").val()).length >= 1) {
                var class_doctor = $("#class_doctor").val();
            } else {
                var class_doctor = "";
            }

            if ($.trim($("#class_room").val()).length >= 1) {
                var class_room = $("#class_room").val();
            } else {
                var class_room = "";
            }

            
            
            if (class_name=="" || class_time=="" || class_doctor=="" || class_room=="") {
                alert("Sorry Missing Some Data");
            }else{

                $.ajax({
                    type: "POST",
                    url: "employee.php",
                    data: {
                        add_class : "",
                        class_name : class_name,
                        class_time : class_time,
                        class_doctor : class_doctor,
                        class_room : class_room
                    },
                    success: function(data){
                        alert("the class has aded \n please refrech the page");
                        // location.reload();
                        }
                });

            }
            

            

        })


        // it will delet a class from the data base
        $(".delete").click(function(){

            var id = $(this).attr('value'); 

            $.ajax({

                type: "POST",
                url: "employee.php",
                data: {
                    remove_class : "",
                    id : id 
                },
                success: function(data){
                    alert("the class has deletid \n please refrech the page");
                }

            });

        })

        

    })


</script>