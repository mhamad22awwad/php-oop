<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>about</title>


    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">

</head>
<body>
        
<?php include "inc/nav.php"; ?>


<div class="container my-5">

    <div class="arabic">

        <div class="card my-3">
            <h2 class="card-title bg-light p-3">ما هذا النظام؟؟</h2>
            <div class="card-body">
                <p>
                    نظام تسجيل طلاب و مواد و جريدة مواد و اضافة مواد لكل طالب
                    مع بقاء بيانات كل طالب لوحده
                    هذا نظام اولي و جزئي
                </p>
            </div>
        </div>
        
        <div class="card my-3">
            <h2 class="card-title bg-light p-3">طريقة العمل به</h2>
            <div class="card-body">
                <p>
                    اذا كنت طالب اضغط على I'm student 
                </p>
                <p>
                    و اذا كنت احد موظفي النظام اضغط على I'm employee 
                </p>
            </div>

        </div>
        
        <div class="card my-3">
            <h2 class="card-title bg-light p-3">اذا كنت طالب</h2>
            <div class="card-body">
                <p>
                    اذا كنت طالب و اخترت الخيار الخاص بك <br>
                    يجب عليك بعدها ان تختار اسم طالب <br>
                    و هكذا سوف تفتح لك صفحة الطالب الذي اخترته <br>
                    ان لم تختر اي طالب فسوف تضهر لك المواد المطروحة من التسجيل ولن تستطسع اضافة  او حذف اي مادة <br>
                    <span class="bg-warning"> ملاحظة: لا يمكن اضافة المادة نفسها مرتين </span>
                </p>
                <p>
                    عند دخولك باسم طالب سوف تستطيع ان تضيف مواد الى جدولك و ان تحذف مواد من جدولك
                </p>
            </div>

        </div>

        <div class="card my-3">
            
            <h2 class="card-title bg-light p-3">اذا كنت موظف</h2>
            <div class="card-body">
                <p>
                    اذا كنت مووظف و اخترت الخيار الخاص بك <br>
                    سوف يضهر لك خيارين
                </p>
                <ol>
                    <li>اضافة مادة</li>
                    <li>اضافة طالب جديد</li>
                </ol>
                <div class="card bg-light p-3 mb-3">
                    <p>
                        اذا اخترت ان تضيف مادة الى جريدة المواد فسوف تذهب الى صفحة جديدة <br>
                        و سوف تقوم بتعبئة بيانات المادة <br>
                        و يمكنك ان تحذف اي مادة لا تريدها في الجريدة <br>
                    </p>
                </div>
                <div class="card bg-light p-3">
                    <p>
                        في حال اردت اضافة طالب جديد <br>
                        سوف يضهر لك صندوق و يجب ان تضع اسم الطالب <br>
                        و عندها سوف يتم اضافة الطالب
                    </p>
                </div>
            </div>

        </div>
        



    </div>



</div>




</body>
</html>