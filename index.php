<?php 
    include "back/functions.php"; 
    $theData = new student();

    if(isset($_POST["btn_add_the_student"])) {
        
        $new_student = mysqli_real_escape_string($conn , $_POST["new_student"]);

        $theData->addStudent($new_student);

    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>log in</title>

    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">


</head>
<body>


<?php include "inc/nav.php"; ?>



<div class="container my-5" id="btn-place">
    <div class="d-flex justify-content-around my-2">
        <button type="button" class="btn w-25 bg-light form-control" id="btn-student">I'm student</button>

        <button type="button" class="btn w-25 bg-light form-control" id="btn-employee">I'm employee</button>
    </div>
    <div class="d-flex justify-content-center">
    <button type="button" class="btn w-50 bg-light form-control" id="btn-restart">restart</button>

    </div>

</div>

<div id="forms">


    <div class="container my-5 d-none" id="student">

        <form action="student.php" method="post">
            
            <div class="form-group">
                <label for="student_name" class="h2">student name</label>
                
                <select name="student_name" class="form-control form-control-lg" id="student_name">
                    
                    <option value="0"> select student </option>
                    <?php $theData->getStudent(); ?>
                    
                </select>
            </div>
            
            <div class="form-group">
                <button type="submit" id="go-to-s-p"  class="form-control w-50 mx-auto">Go to Sutdent Page</button>
            </div>

        </form>

    </div>

    <div class="container d-none my-5"  id="employee" >

        <div class="d-flex justify-content-around my-2 mb-5" id="adds">

            <button type="button" id="btn-new-class" class="btn w-25 bg-light form-control">Add Class</button>

            <button type="button" id="btn-new-student" class="btn w-25 bg-light form-control">Add Student</button>
            
        </div>

        <div class="d-none" id="add-new-student">
            <form action="" method="post">
                
                <div class="form-group">
                    <label for="new-student" class="h2">The new Student Name</label>
                    <input type="text" class="form-control form-control-lg" name="new-student" id="new-student" placeholder="Student Name">
                </div>
                <div>
                    <button type="button" id="btn-add-the-student" class="form-control w-50 mx-auto">Add The Student</button>
                </div>
            </form>
        </div>

    </div>
    


</div>


</body>
</html>

<script>

$(document).ready(function(){

    // onClick the form for student will display (show)
    // and the employee form will stay hiden
    $("#btn-student").click(function(){

        $("#student").removeClass("d-none");
        $("#employee").addClass("d-none");
        
    });


    // onClick the form for employee will display (show)
    // and the student form will stay hiden
    $("#btn-employee").click(function(){

        $("#employee").removeClass("d-none");
        $("#student").addClass("d-none");

    });


    // it will hide the both forms
    $("#btn-restart").click(function(){

        $("#student").addClass("d-none");
        $("#employee").addClass("d-none");
        $("#add-new-student").addClass("d-none");

    });

    // it will chick if you select any user name
    $("#go-to-s-p").click(function(){

        // get the name of the student you select
        var selectid = $('#student_name').find(":selected").text();

        // get the val (id) of the student you select
        var selectid_val = $("#student_name").val();

        if ( selectid_val > 0 ) {
            alert("Hello " + selectid );
        } else {
            alert("Please select your name");
            // window.location.href = "http://localhost/tast/";
        }
        

    });

    // 
    $("#btn-new-class").click(function(){

        alert("welcome to employee page")
        window.location.href = "http://localhost/tast/employee.php";
    });

    $("#btn-new-student").click(function(){

        $("#add-new-student").removeClass("d-none");
    
    });


    $("#btn-add-the-student").click(function(){


        if ($.trim($("#new-student").val()).length >= 1) {
            var new_student = $("#new-student").val();
        } else {
            var new_student = "";
        }

        if (new_student == "") {
            alert("missing the name");
        }else{

            $.ajax({
                type: "POST",
                url: "index.php",
                data: {
                    btn_add_the_student : "",
                    new_student : new_student
                },
                success: function(){
                    alert("the new Student is added \n please refrech the page");
                }
            });

        }
        

    });

});


</script>