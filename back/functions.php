<?php
include "conn.php";


/**
 * 
 */
class employee {


    /**
     * it will return the times avaiabel for classes
     * and will return $rowTime
     * ready to use
    */
    public function getTime(){
        global $conn;

        // get the data from the data base
        $sql = "SELECT * FROM times ";

        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            
            while($rowTime=$result->fetch_assoc()){
                echo "<option value='".$rowTime['id']."'>".$rowTime['time']."</option> ";
            }

        } else {
            echo "there is no data";
        }


    }


    /**
     * it will return all doctors name
     * and will return $rowDoctor
     * ready to use
    */
    public function getDoctor(){
        global $conn;

        $sql = "SELECT * FROM doctors ";

        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            
            while($rowDoctor=$result->fetch_assoc()){
                echo "<option value='".$rowDoctor["id"]."'>".$rowDoctor["doctor_name"]."</option> ";
            }

        } else {
            echo "there is no data";
        }
        
    }


    /**
     * it will return all rooms locations
    */
    public function getRoom(){
        global $conn;

        $sql = "SELECT * FROM rooms ";

        $result = $conn->query($sql);
        
        if ($result->num_rows > 0) {
            
            while($rowRoom=$result->fetch_assoc()){
                echo "<option value='".$rowRoom["id"]."'>".$rowRoom["location"]."</option> "; 
            }

        } else {
            echo "there is no data";
        }
        
    }


    /**
     * it will return all class 
     * ready to use
    */
    public function getClass(){
        global $conn;

        $sql = "SELECT * FROM class c, times t ,doctors d , rooms r
            WHERE c.class_time = t.id
            and c.class_doctor = d.id
            and c.class_room   = r.id
            ORDER BY class_time ,class_room ASC
        ";

        $result = $conn->query($sql);
        $output = "

            <h2 class='text-center'> here is all the Lessons in these Semister you add </h2>
            <table class='w-100 table table-hover'>
                <thead>
                    <tr>
                        <td>class name</td>
                        <td>class time</td>
                        <td>class teacher</td>
                        <td>class room</td>
                        <td>delet</td>
                    </tr>
                </thead>

                <tbody>
        ";
        if ($result->num_rows > 0) {
            
            while($rowClass=$result->fetch_assoc()){

                $output .= " <tr>
                                <td>".$rowClass["class_name"]."  </td>
                                <td>".$rowClass["time"]."  </td>
                                <td>".$rowClass["doctor_name"]."</td>
                                <td>".$rowClass["location"]."  </td>
                                <td><button type='button' class='delete btn btn-outline-danger' name='remove_class' value='".$rowClass["class_id"]."' id='delete".$rowClass["class_id"]."'>delete</button></td>
                            </tr>";

            }
            $output .="
                    </tbody>
                </table>
            " ;
            
        } else {

            $output = "<h1>Sorry no any class</h1> ";
            
        }
        
        echo $output;
    }


    /**
     * thes function will add class to the data base
     * ready to use
    */
    public function addClass($class_name, $class_time, $class_doctor, $class_room) {
        global $conn;
        
        $sql = "INSERT INTO class (class_name, class_time, class_doctor, class_room) 
        VALUES ('$class_name','$class_time','$class_doctor','$class_room') ";

        if ($conn -> multi_query($sql)==TRUE) {
			echo "data Inserted"."<br>";
		} else {
			echo "error : ".$sql."<br>".$conn->error;
		}

    }

    /**
     * these function will delet the class in the data base
     * ready to use
     */
    public function removeClass($id){
        global $conn;

        $sql = "DELETE FROM class WHERE class_id='$id' ";
        $conn->multi_query($sql);

    }

}


/**
 * 
 */
class student extends limits{


    /**
     * it will show all the student in the data base
     * ready to use
    */
    public function getStudent(){
        global $conn;

        $sql = "SELECT * FROM students ";

        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            
            while($row=$result->fetch_assoc()){
                echo "<option value='".$row["student_id"]."'>".$row["student_name"]."</option> ";
            }

        } else {
            echo "there is no doctor";
        }
        

    }


    /**
     * it will display the name of the enterd student
     * ready to use
    */
    public function getOneStudent($id){
        global $conn;

        $sql = "SELECT * FROM students WHERE student_id='$id' ";

        $result = $conn->query($sql);

        $user;
        if ($result->num_rows > 0) {
            
            while($row=$result->fetch_assoc()){
                $user = "<span>".$row["student_name"]."</span> ";
            }

        }else{
            $user = "<span>:You are not loged in </span>";
        }

        echo $user;

    }


    /**
     * this function will show all class in the data base
     * if you login a true user name you will see all the class you in
     * and you will be abel to add or remove any class 
     * 
     * but if you enter witawt any user name 
     * you cant change (add or remove) any class 
     * ready to use
    */
    public function getClass(){
        global $conn;
        global $id_student;

        $sql = "SELECT * FROM class c, times t ,doctors d , rooms r
            WHERE c.class_time = t.id
            and c.class_doctor = d.id
            and c.class_room   = r.id
            ORDER BY class_time ,class_room ASC
        ";

        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            if ($id_student>0) {
                $output = "
                    <h1 class='text-center'>Here are Lessons you can take</h1>

                    <table class='w-100 table table-hover'>
                    
                    <thead>
                        <tr>
                            <td>class name</td>
                            <td>class time</td>
                            <td>class teacher</td>
                            <td>class room</td>
                            <td>add class</td>
                        </tr>
                    </thead>

                    <tbody>
                ";
                while($rowClass=$result->fetch_assoc()){

                    $output .= "<tr>
                                <td>".$rowClass["class_name"]."  </td>
                                <td>".$rowClass["time"]."  </td>
                                <td>".$rowClass["doctor_name"]."</td>
                                <td>".$rowClass["location"]."  </td>
                                <td><button type='button' class='add-class btn btn-outline-success' name='add_class' value='".$rowClass["class_id"]."' id='add_class".$rowClass["class_id"]."'>add class</button></td>
                            </tr>";

                }
                $output .= "
                                </tbody>
                            </table>
                ";
            }else{
                
                $output = "
                    <h1 class='text-center'>Here are Lessons you can take <span class='bg-warning'>AFTER</span> log in</h1>
                        <table class='w-100 table table-hover'>
                        
                        <thead>
                            <tr>
                                <td>class name</td>
                                <td>class time</td>
                                <td>class teacher</td>
                                <td>class room</td>
                            </tr>
                        </thead>

                        <tbody>
                ";
                while($rowClass=$result->fetch_assoc()){

                    $output .= "<tr>
                                <td>".$rowClass["class_name"]."  </td>
                                <td>".$rowClass["time"]."  </td>
                                <td>".$rowClass["doctor_name"]."</td>
                                <td>".$rowClass["location"]."  </td>
                            </tr>";

                }
                $output .= "
                            </tbody>
                        </table>
                ";

            }
            
            
        } else {

            $output = "<tr> Sorry no any class </tr> ";
        
        }

        echo $output;
        
    }


    /**
     * it will show all the class for one student
     * and till you the limet you can add
     * and will till you how many class you have 
     * ready to use
    */
    public function showClassStudent($id){
        global $conn;

        // till you how many are the class you in
        $num = $this->numClassStudent($id);


        $sql = "SELECT * FROM student_class sc , class c, times t ,doctors d , rooms r
            WHERE sc.id_student = '$id' 
            and sc.id_class = c.class_id 
            and c.class_time = t.id
            and c.class_doctor = d.id
            and c.class_room   = r.id
        ";

        $result = $conn->query($sql);
        
        $output = "
        <h1 class='text-center'>Lessons you are Enrolled in</h1>
            <table class='w-100 table table-hover'>
                <thead>
                    <tr>
                        <td>class name</td>
                        <td>class time</td>
                        <td>class teacher</td>
                        <td>class room</td>
                        <td>remove class</td>
                    </tr>
                </thead>
            <tbody>
        ";
        if ($result->num_rows > 0) {
            while($row=$result->fetch_assoc()){
                
                $output .= "<tr>
                                <td>".$row["class_name"]."  </td>
                                <td>".$row["time"]."  </td>
                                <td>".$row["doctor_name"]."</td>
                                <td>".$row["location"]."  </td>
                                <td><button type='button' class='remove-class btn btn-light btn-outline-danger' name='remove_class' value='".$row["sc_id"]."' id='remove-class".$row["class_id"]."'>remove class</button></td>
                            </tr>";

            }

            $output .= "
                            </tbody>
                        </table>
                        <h2>you have ".$num." Lessons in this Semister</h2>
                        <h4>Note: the limit for Lessons in the Semister is 6 Lessons</h4>
                    ";
        }else {

            $output = "";
        
        }

        echo $output;

    }


    /**
     * it will chiek for the limit (6) and if you have 6 class added
     * you want be abel to add another class
     * and will show you all of you class
     * ready to use
    */
    public function addClassStudent( $id_class , $id_student ){
        global $conn;
        
        // see how much class you have
        $num = $this->numClassStudent($id_student);

        // set the limet
        $limit = 6;

        // chik the limit
        if ($num < $limit) {
            
            // you are under the limit
            // and you can add class
                        
            $sqlSelect = "SELECT * FROM class WHERE class_id = '$id_class'";
            $resultSelect = $conn->query($sqlSelect);
            if ($resultSelect->num_rows > 0) {
            
                while($rowSelect=$resultSelect->fetch_assoc()){
                    $class_time = $rowSelect["class_time"];
                    $class_doctor = $rowSelect["class_doctor"];
                    $class_room = $rowSelect["class_room"];
                    
                }
            
            }


            // $sum = (string)$id_class . (string)$class_time . (string)$class_doctor . (string)$class_room . (string)$id_student;
            $sum = (string)$id_class . (string)$id_student;

            $sql = "INSERT INTO student_class (id_class , id_student , class_student) 
                VALUES ( '$id_class' , '$id_student' , '$sum' )
            ";

            if ($conn -> multi_query($sql)==TRUE) {
                $outputMesage = "data Inserted"."<br>";
            } else {
                echo "error : ".$sql."<br>".$conn->error;
            }

        } else {
            // you have reache the limit
            // and you cant add another class
            $outputMesage = "you reatch the lemit";
        }
        

        
        return $outputMesage ;

    }


    /**
     * it will delet the class you dont want
     * ready to use
    */
    public function removeClassStudent($id){
        global $conn;


        $sql = "DELETE FROM student_class WHERE sc_id='$id' ";
        $conn->multi_query($sql);

    }


    /**
     * it will add a new student to the data base
     * ready to use
    */
    public function addStudent($student_name){
        global $conn;


        $sql = "INSERT INTO students (student_name) 
        VALUES ('$student_name') ";

        if ($conn -> multi_query($sql)==TRUE) {
			echo "data Inserted"."<br>";
		} else {
			echo "error : ".$sql."<br>".$conn->error;
		}
    }



}


/**
 * 
 */
class limits {


    /**
     * it will calculat the num of class for the student
    */
    public function numClassStudent($id){
        global $conn;


        $sql = "SELECT * FROM student_class WHERE id_student = '$id' ";

        $result = $conn->query($sql);
        $sum = 0;
        if ($result->num_rows > 0) {
            
            $sum = 0;
            
            while($row=$result->fetch_assoc()){

                $sum++;

            }

        }

        return $sum ;
    }



}
