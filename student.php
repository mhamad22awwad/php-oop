<?php
    include "back/functions.php"; 
    $theData = new student();

    $theLimits = new limits();

    session_start();

    $id_student = $_SESSION["id_student"] = mysqli_real_escape_string($conn , $_POST["student_name"]);

    $id_student = (int)$id_student;


    if(isset($_POST["add_class"])){
        
        $id_student = mysqli_real_escape_string($conn , $_POST["id_student"]);
        $id_class = mysqli_real_escape_string($conn , $_POST["id_class"]);
        
        $theData->addClassStudent($id_class,$id_student);
        
    }

    if (isset($_POST["remove_class"])) {
        
        $id = mysqli_real_escape_string($conn , $_POST["id"]);
        $theData->removeClassStudent($id);
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>student</title>

    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">

</head>
<body>


<?php include "inc/nav.php"; ?>



<div class="container">

    <div>
        <h1> Hello <?php $theData->getOneStudent($id_student); ?> </h1>
    </div>

    <div class="col-lg-12 my-5">
        
        
            <?php $theData->showClassStudent($id_student); ?>
            
    </div>

    <div class="col-lg-12 my-5">
        
        
                <?php $theData->getClass(); ?>

    </div>

    <div class="text-center my-5">
        <a href="index.php" class="btn btn-danger w-25">Back</a>
    </div>

</div>


    
</body>
</html>


<script>

$(document).ready(function(){


    // it will remove class from studnt tabel
    $(".remove-class").click(function(){
        var id = $(this).attr("value");

        $.ajax({
            type: "POST",
            url: "student.php",
            data: {
                remove_class : "",
                id : id 
            },
            success: function(data){
                alert("the class has removed \n please refrech the page");
            }
        });
    });


    // it will add class to the student tabel
    $(".add-class").click(function(){
        var id_class = $(this).attr('value'); 

        $.ajax({
            type: "POST",
            url: "student.php",
            data: {
                add_class : "",
                id_class : id_class,
                id_student : <?php echo $id_student; ?>
            },
            success: function(data){
                if (<?php echo $theLimits->numClassStudent($id_student) ?> < 6) {
                    alert("the class has aded \n please refrech the page");
                } else {
                    alert("Sorry you ritche the limit ");
                }
                
            }
        });
    });


    
    


})






</script>